# Dup Finder

Go module to find and list duplicate files (primarily developed for managment of my personal tv/movie collection).
Uses a sqlite3 local db (created in the running directory) to handle data enumeration and comparison.
Uses imohash for quick and reliable(?) hashing.

## How to use
- download the `dup-finder` artifact as built by project pipelines; or download the repo and run `go build`
- run `./dup-finder` to see the program run against the provided test-data
- alternatively
    - Run `./dup-finder --dir /my/data/dir`
    - Run with screen for large directories: `screen ./dup-finder --dir /mnt/media/movies`
    - Run with the `--full-output` flag for two output files, one for duplicates-only, one for a full list of files: `./dup-finder --dir /mnt/media/movies --full-output --debug`


## A note on speed
I've not performed any benchmarking (yet), but after a journey from sha512 through sha256 and sha1 I finally landed at imohash which feels pretty well suited to my use-case (many video file s@ >500mb each).
With that in mind this seems impressive (again, assuming imohash is accurate/reliable): `/mnt/media/movies` is a 23T directory stored on a spinning-disk nas (unraid).


```bash
level=info ts=2021-09-17T19:45.00.00 caller=main.go:388 msg="Analysis directory: /mnt/media/movies"
...
level=info ts=2021-09-17T19:50:00.550314354Z caller=main.go:116 msg="|| Beginning duplicate file analysis ||"
level=info ts=2021-09-17T19:50:45.047229913Z caller=main.go:368 msg="Files analyzed: 15298"
level=info ts=2021-09-17T19:50:45.047403661Z caller=main.go:372 msg="Duplicates found: 3672"
level=info ts=2021-09-17T19:50:45.070067303Z caller=main.go:387 msg="Process took 5m10.270011078s"
```


### Thanks to
github.com/kalafut/imohash for the neat and fast hashing tool.
