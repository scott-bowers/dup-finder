module gitlab.com/sbowers.sequoia/dup-finder

go 1.17

require (
	github.com/kalafut/imohash v1.0.2
	github.com/mattn/go-sqlite3 v1.14.9
	github.com/rs/zerolog v1.26.1
)

require github.com/twmb/murmur3 v1.1.5 // indirect
