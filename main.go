package main

import (
	"database/sql"
	"encoding/json"
	"flag"
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/kalafut/imohash"
	"github.com/mattn/go-sqlite3"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

// wp: in an ideal implementation would these two variables be defined in main.go and then passed around (as part of a struct perhaps?)?
var DB *sql.DB

//-------------------
// Helper Functions
//-------------------

// initialize takes care of Logging config, CLI flag parsing, and initial Database connection
func initialize() (dir *string, fullOutputFlag *bool, err error) {
	// Initialize Logging
	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix

	// Take flags for
	//   debug logging
	debug := flag.Bool("debug", false, "sets log level to debug")
	//   target directory
	dir = flag.String("dir", "./test-data", "a path string")
	//   full output option
	fullOutputFlag = flag.Bool("full-output", false, "a full list output bool; defaults to false 'no output of full list'")

	// Parse flags
	flag.Parse()

	// Set global level to Info
	zerolog.SetGlobalLevel(zerolog.InfoLevel)
	// Set global level to Debug if `--debug` flag was supplied
	if *debug {
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	}

	// Initialize logger
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})
	log.Info().Msg("Logging initialized")
	log.Debug().Msg("Logging config allows debug")

	// Initialize database configuration
	sql.Register("dup-finder", &sqlite3.SQLiteDriver{})
	connectDB()

	// validate database configuration
	if err := DB.Ping(); err != nil {
		return dir, fullOutputFlag, err
	}
	return dir, fullOutputFlag, nil
}

// connectDB has statically defined inputs for the local sqlite3 database `./dufi.db`
func connectDB() error {
	db, err := sql.Open("sqlite3", "./dufi.db")
	db.SetMaxOpenConns(24)
	db.SetMaxIdleConns(6)
	log.Info().Msg("Sqlite3 database connection established...")
	DB = db
	return err
}

func processTx(sqlString string) error {
	// open transaction
	tx, err := transactionBegin(DB)
	if err != nil {
		return err
	}
	defer tx.Commit()

	// prep statement
	stmt, err := DB.Prepare(sqlString)
	if err != nil {
		return err
	}
	defer stmt.Close()

	// execute statement
	_, err = stmt.Exec()
	if err != nil {
		return err
	}

	// close statment
	if err = stmt.Close(); err != nil {
		return err
	}

	// commit transaction
	if err = tx.Commit(); err != nil {
		return err
	}
	return nil
}

// transactionBegin is a wrapper (that's barely useful) for opening transactions
func transactionBegin(db *sql.DB) (tx *sql.Tx, err error) {
	tx, err = db.Begin()
	if err != nil {
		return nil, err
	}
	return tx, nil
}

// countRows is a wrapper for counting the rows of a sql table (but its kinda )
func countRows(qs string) (c int, err error) {

	tx, err := DB.Begin()
	if err != nil {
		// wp: is `-1` a really bad idea here? or since I'm bubbling up the error (and not operating on the `c int` return value) is it okay?
		return -1, err
	}

	var count int

	if err = tx.QueryRow(qs).Scan(&count); err != nil {
		// wp: is `-1` a really bad idea here? or since I'm bubbling up the error (and not operating on the `c int` return value) is it okay?
		return -1, err
	}
	return count, nil
}

//-------------------
// Primary Logic
//-------------------

// findDupes is the bloated function that a) creates DB tables, b) walks the filepath calling the 'hashWalk' function on each file item
func findDupes(dir *string, full *bool) error {
	// capture time
	start := time.Now()

	// create our two db tables
	if err := processTx("CREATE TABLE IF NOT EXISTS files (path TEXT PRIMARY KEY, hex TEXT)"); err != nil {
		return err
	}
	if err := processTx("CREATE TABLE IF NOT EXISTS duplicates (hex TEXT PRIMARY KEY, paths TEXT)"); err != nil {
		return err
	}

	// log targeted directory
	log.Info().Msg("Analysis directory: " + *dir)

	// walk the target directory and calculate hashes
	if err := filepath.Walk(*dir, hashWalk); err != nil {
		return err
	}

	// log that the walk has completed and db-analysis begins
	log.Info().Msg("|| Beginning duplicate file analysis ||")

	// run our file hash-compare and catalogue function
	if err := identifyDupes(); err != nil {
		return err
	}

	// count the rows of the `files` table of the DB to see how many files we had total
	filecount := "SELECT COUNT(*) FROM files"
	fc, err := countRows(filecount)
	if err != nil {
		return err
	}
	log.Info().Msgf("Files analyzed: %d", fc)

	// count the rows of the `duplicates` table of the DB to see how many files have two or more entries
	duplicatecount := "SELECT COUNT(*) FROM duplicates"
	dc, err := countRows(duplicatecount)
	if err != nil {
		return err
	}
	log.Info().Msgf("Duplicates found: %d", dc)

	// use the getJSON function to pull well-formatted data from the `duplicates` table
	j, err := getJSON("SELECT * FROM duplicates")
	if err != nil {
		return err
	}

	// write that well-formatted JSON data to a file
	if err = os.WriteFile("./duplicates-output.json", j, 0666); err != nil {
		return err
	}

	// if the `--full` flag was specified, also provide a wel-formatted JSON file output of all files scanned
	if *full {
		j, err := getJSON("SELECT * FROM files")
		if err != nil {
			return err
		}
		if err = os.WriteFile("./files-output.json", j, 0666); err != nil {
			return err
		}
	}

	// close our db connection
	DB.Close()
	log.Info().Msg("Sqlite3 database connection closed.")

	// profiling, sorta
	elapsed := time.Since(start)
	log.Info().Msg("Process took " + elapsed.String())

	// get outta here
	return nil
}

// hashWalk takes a path and a file, calculates the hash of that file and stores the k:v {path:hex} in the Files db
//  inputs and outputs for this function are determined by its caller (aka don't change anything about the func definition line)
//  https://pkg.go.dev/path/filepath#WalkFunc
func hashWalk(path string, info os.FileInfo, err error) error {
	// why am I checking for an error here? I guess if filepath.walk chokes on open?
	// TODO: experiment with this or look up documentation
	if err != nil {
		return err
	}

	// if directory do nothing
	if info.IsDir() {
		return nil
	}

	// TODO: consider allowing passthrough of hashing parameters
	log.Info().Msg("Hashing file: " + path)
	hash, err := imohash.SumFile(path)
	if err != nil {
		return err
	}

	// take the hex as a string
	hex := fmt.Sprintf("%x", hash)

	// open transaction
	tx, err := DB.Begin()
	if err != nil {
		return err
	}

	// create an entry in the `files` table with a primary key of `path` and a column for `hex`
	//		TODO: remember/relearn why the `DO UPDATE SET` is necessary/preferred
	//			  does that just allow re-use of the scanned file-list with updated hexs (since filenames are already unique)?
	log.Debug().Msg("Attempting insert of: " + hex)
	// prepare statement
	stmt, err := DB.Prepare("INSERT INTO files (path, hex) VALUES (?, ?) ON CONFLICT(path) DO UPDATE SET hex=?")
	if err != nil {
		return err
	}

	// execute statement
	if _, err = stmt.Exec(path, hex, hex); err != nil {
		return err
	}

	// close statement
	if err = stmt.Close(); err != nil {
		return err
	}

	// commit transaction
	if err = tx.Commit(); err != nil {
		return err
	}

	return nil
}

// identifyDupes operates on the contents of the Files DB and performs comparisons to catalogue duplicate entries by hex
func identifyDupes() error {

	type Row struct {
		path string
		hex  string
	}

	type Dups struct {
		hex   string
		paths []string
	}

	tx, err := transactionBegin(DB)
	if err != nil {
		return err
	}
	defer tx.Commit()

	rows, err := tx.Query("select * from files")
	if err != nil {
		return err
	}

	defer rows.Close()

	for rows.Next() {
		var row Row

		if err := rows.Scan(&row.path, &row.hex); err != nil {
			return err
		}

		dupRows, err := tx.Query("select hex, path from files where hex = ?", row.hex)
		if err != nil {
			return err
		}
		defer dupRows.Close()

		var dups Dups

		for dupRows.Next() {
			var p string
			if err := dupRows.Scan(&dups.hex, &p); err != nil {
				return err
			}
			log.Debug().Msg("Primary File Path: " + row.path)
			log.Debug().Msg("Dup Path: " + p)
			dups.paths = append(dups.paths, p)
		}

		list := strings.Join(dups.paths, ",")
		log.Debug().Msgf("Duplicate Paths: %+v", list)
		if len(dups.paths) > 1 {
			stmt, err := tx.Prepare("INSERT INTO duplicates (hex, paths) VALUES (?, ?) ON CONFLICT(hex) DO UPDATE SET paths=?")
			if err != nil {
				return err
			}

			if _, err = stmt.Exec(dups.hex, list, list); err != nil {
				return err
			}

			if err = stmt.Close(); err != nil {
				return err
			}

			if err = dupRows.Err(); err != nil {
				return err
			}
		}
	}

	if err = rows.Err(); err != nil {
		return err
	}

	if err = rows.Close(); err != nil {
		return err
	}

	if err = tx.Commit(); err != nil {
		return err
	}
	return nil
}

// getJSON loops through a databse table encoding its contents as 'pretty' json, provide the sql string "SELECT * FROM table" mostly stolen from https://stackoverflow.com/a/29164115
func getJSON(sqlString string) ([]byte, error) {
	rows, err := DB.Query(sqlString)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	columns, err := rows.Columns()
	if err != nil {
		return nil, err
	}

	count := len(columns)
	tableData := make([]map[string]interface{}, 0)
	values := make([]interface{}, count)
	valuePtrs := make([]interface{}, count)

	for rows.Next() {
		for i := 0; i < count; i++ {
			valuePtrs[i] = &values[i]
		}
		rows.Scan(valuePtrs...)
		entry := make(map[string]interface{})
		for i, col := range columns {
			var v interface{}
			val := values[i]
			b, ok := val.([]byte)
			if ok {
				v = string(b)
			} else {
				v = val
			}
			entry[col] = v
		}
		tableData = append(tableData, entry)
	}

	jsonData, err := json.MarshalIndent(tableData, "", "    ") // pretty
	// jsonData, err := json.Marshal(tableData) // not pretty

	if err != nil {
		log.Error().Err(err).Msg("Failed to marshal and indent pretty json.")
		return nil, err
	}
	return jsonData, nil
}

func main() {
	dir, fullOutputFlag, err := initialize()
	if err != nil {
		log.Fatal().Err(err).Msg("Initilization failed.")
	}

	if err := findDupes(dir, fullOutputFlag); err != nil {
		log.Fatal().Err(err).Msg("Uncaught exception, good luck nerd!")
	}
}
